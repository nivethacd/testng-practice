import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;

public class ListenerTest extends Login implements ITestListener {
    @Override
    public void onTestSuccess(ITestResult arg0) {
        // For valid login credentials, assert that user successfully redirects to Home Page.
    }

    @Override
    public void onTestFailure(ITestResult result) {
        ITestContext context = result.getTestContext();
        WebDriver driver = (WebDriver)context.getAttribute("WebDriver");

        // For invalid login credentials, take a screenshot of the error message
        TakesScreenshot ts = (TakesScreenshot)driver;

        //capture screenshot as output type FILE
        File file = ts.getScreenshotAs(OutputType.FILE);

        try {
            //save the screenshot taken in destination path
            FileUtils.copyFile(file, new File("Nivetha.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("The screenshot is taken");

    }
}
