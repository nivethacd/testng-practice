import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners(ListenerTest.class)
public class Login {

    public static WebDriver driver;

    @BeforeMethod
    public static void setUp(ITestContext context) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        context.setAttribute("WebDriver", driver);
    }

    @DataProvider(name = "data-provider")
    public Object[][] dpMethod() {
        return new Object[][]{
                {"nivetha@gmail.com", "nivetha16@"},
        };
    }

    //valid credentials
    @Test(dataProvider = "data-provider")
    public void loginTest(String userName, String password) {
        //define the url
        driver.get("http://practice.automationtesting.in/");
        //maximize the window
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //get the title of the webpage
        String pageTitle = driver.getTitle();
        System.out.println("The title of this page is ===> " + pageTitle);
        //enter the locator of username and clear the input field before entering any value
        driver.findElement(By.id("menu-item-50")).click();
        //enter the username
        driver.findElement(By.id("username")).sendKeys(userName);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.name("login")).click();
        driver.findElement(By.xpath("//*[@id=\"page-36\"]/div/div[1]/nav/ul/li[2]/a")).click();
    /*String actualUrl="http://practice.automationtesting.in/my-account/orders/";
    String expectedUrl= driver.getCurrentUrl();
    Assert.assertEquals(expectedUrl,actualUrl);*/
        // driver.findElement(By.linkText("Logout"));
        //wait for the page to load
        // driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
    }
    
    //invalid credentials
    @Test
    public void loginto() {
        //define the url
        driver.get("http://practice.automationtesting.in/");
        //maximize the window
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //get the title of the webpage
        String pageTitle = driver.getTitle();
        System.out.println("The title of this page is ===> " + pageTitle);
        //enter the locator of username and clear the input field before entering any value
        driver.findElement(By.id("menu-item-50")).click();
        //enter the username
        driver.findElement(By.id("username")).sendKeys("test");
        driver.findElement(By.id("password")).sendKeys("test");
        driver.findElement(By.name("loogin")).click(); //wrong name, hence error
        driver.findElement(By.xpath("//*[@id=\"page-36\"]/div/div")).click();
    }

    @AfterMethod
    public static void tearDown() {
        driver.quit();
    }
}
